app1 adalah sebuah service sederhana yang menangani master product. saya step-by-step mengikuti tutorial dari sumber yang telah saya cantumkan di bawah. namun ada beberapa perubahan dan penyempurnaan yang saya lakukan.

Ada beberapa field yang perlu diketahui:
1. productid
- Id dari sebuah produk.
- Integer.

2. productname
- Nama dari sebuah produk.
- String.

3. productprice
- Harga dari sebuah produk.
- Float.

4. productstock
- Stok dari sebuah produk.
- Integer

Dengan menggunakan postman, ada 4 REST API di sini:
1. GET
- Untuk melihat sebuah data produk.
- productid wajib diisi
- api: localhost:5000/product/{productid} -> Method GET
- contoh: localhost:5000/product/1
- hasil: { "id": 1, "name": "\"ayam sayur\"", "price": 100.0, "stock": 100 }


2. PUT
- Untuk menambah sebuah data produk.
- Seluruh data wajib diisi
- api: localhost:5000/product/{productid}/ -> Method PUT
- link: localhost:5000/product/{productid}?name={productname}&price={productprice}&stock={productstock}
- contoh: localhost:5000/product/4?name=Bakmi&price=50&stock=50
- hasil: { "id": 4, "name": "Bakmi", "price": 50.0, "stock": 50 }
- id tidak bisa duplikat

3. PATCH
- Untuk memperbarui sebuah data produk.
- Seluruh data opsional diisi, kecuali productid
- api: localhost:5000/product/{productid}/ -> Method PATCH
- link: localhost:5000/product/{productid}?name={productname}&price={productprice}&stock={productstock}
- contoh: localhost:5000/product/4?name=Sapi&price=50&stock=50
- hasil: { "id": 4, "name": "Sapi", "price": 50.0, "stock": 50 }

4. DELETE
- Untuk menghapus sebuah data produk.
- api: localhost:5000/product/{productid} -> Method DELETE
- contoh: localhost:5000/product/1
- hasil: { "id": 1, "name": "\"ayam sayur\"", "price": 100.0, "stock": 100 }
- pada run pertama, hasil akan menunjukkan datanya, lalu jalankan lagi, menunjukkan produk tidak ada.

Sumber  :
https://www.youtube.com/watch?v=GMppyAPbLYk&ab_channel=TechWithTim