from flask import Flask, request
from flask_restful import Api, Resource, reqparse, abort, fields, marshal_with
from flask_sqlalchemy import SQLAlchemy, Model

app = Flask(__name__)
api = Api(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database1.db'
db = SQLAlchemy((app))
db.create_all()


class ProductModel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    price = db.Column(db.Float, nullable=False)
    stock = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return f"Product(name={name}, price={price}, stock={stock})"

# if no db
# db.create_all()


product_put_args = reqparse.RequestParser()
product_put_args.add_argument(
    "name", type=str, help="Name of the product is required", required=True)
product_put_args.add_argument(
    "price", type=float, help="Price of the product is required", required=True)
product_put_args.add_argument(
    "stock", type=int, help="Stock of the product is required", required=True)

product_patch_args = reqparse.RequestParser()
product_patch_args.add_argument(
    "name", type=str, help="Name of the product is required")
product_patch_args.add_argument(
    "price", type=float, help="Price of the product is required")
product_patch_args.add_argument(
    "stock", type=int, help="Stock of the product is required")

resource_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'price': fields.Float,
    'stock': fields.Integer
}


class Products(Resource):
    @marshal_with(resource_fields)
    def get(self, product_id):
        result = ProductModel.query.filter_by(id=product_id).first()
        if not result:
            abort(404, message="Product not exists")
        return result

    @marshal_with(resource_fields)
    def put(self, product_id):
        args = product_put_args.parse_args()
        result = ProductModel.query.filter_by(id=product_id).first()
        if result:
            abort(409, message="Product id already exists")
        product = ProductModel(
            id=product_id, name=args['name'], price=args['price'], stock=args['stock'])
        db.session.add(product)
        db.session.commit()
        return product, 201

    @marshal_with(resource_fields)
    def patch(self, product_id):
        args = product_patch_args.parse_args()
        result = ProductModel.query.filter_by(id=product_id).first()
        if not result:
            abort(404, message="Product not exists")

        if args['name']:
            result.name = args['name']
        if args['price']:
            result.price = args['price']
        if args['stock']:
            result.stock = args['stock']

        # db.session.commit()
        return result

        # db.session.update(result)
        # db.session.commit()
        return result

    @marshal_with(resource_fields)
    def delete(self, product_id):
        result = ProductModel.query.filter_by(id=product_id).first()
        if not result:
            abort(404, message="Product not exists")
        db.session.delete(result)
        db.session.commit()
        return result


api.add_resource(Products, "/product/<int:product_id>")

if __name__ == "__main__":
    app.run(debug=True)
